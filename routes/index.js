var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  let version = '1.0.1'
  res.send({version});
});

module.exports = router;
