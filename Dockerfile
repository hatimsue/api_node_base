FROM node:10.17.0-jessie-slim

RUN mkdir app
WORKDIR app
COPY . .
RUN npm update && npm install
CMD [ "npm", "start"]